import React from "react";
import ReactDOM from "react-dom";

import "./style_global.scss";
import RegisterTrials from "./pages/register-trials";

import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<RegisterTrials />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
