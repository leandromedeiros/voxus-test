import React, { Fragment } from "react";
import "./style.scss";

import Header from "../../components/Header";
import Logo from "../../components/Logo";
import Section from "../../components/Section";
import Title from "../../components/Title";
import Text from "../../components/Text";

import FormBox from "../../components/FormBox";
import InputText from "../../components/InputText";
import Button from "../../components/Button";

function RegisterTrials() {
  return (
    <Fragment>
      <Header>
        <Logo />
      </Header>
      <Section variation="register-page">
        <div className="row --align-center">
          <div className="col-md-7 col-sm-12">
            <Title text="Você está a poucos passos de otimizar suas campanhas" />
            <Text text="Queremos conhecer mais sobre sua empresa para melhorar sua experiência com a Voxus." />

            <FormBox>
              <InputText placeholder="Nome completo" />
              <InputText placeholder="Telefone" />
              <InputText placeholder="Endereço de e-mail" />
              <InputText placeholder="Nome da empresa" />
              <InputText placeholder="URL do site" />
              <Button text="Experimentar gratuitamente" />
            </FormBox>
          </div>
        </div>
      </Section>
    </Fragment>
  );
}

export default RegisterTrials;
