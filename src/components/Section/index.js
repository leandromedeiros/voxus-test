import React from "react";
import "./style.scss";

function Section({ variation, children }) {
  return (
    <section className={`section-default ${variation}`}>
      <div className="container">{children}</div>
    </section>
  );
}

export default Section;
