/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from "react";
import "./style.scss";

function InputText({ placeholder }) {
  const generateId = () => new Date().getTime();

  const id = generateId();

  const [active, useActive] = useState(false);
  const [field, useField] = useState("");

  return (
    <div className="input-text">
      <label htmlFor={id} className={`label ${active && "-active"}`}>
        {placeholder}
      </label>
      <input
        id={id}
        className="field"
        value={field}
        onChange={e => useField(e.target.value)}
        onFocus={() => useActive(true)}
        onBlur={() => !field && useActive(false)}
      />
    </div>
  );
}

export default InputText;
