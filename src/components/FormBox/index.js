import React from "react";
import "./style.scss";

function FormBox({ children }) {
  return <div className="form-box">{children}</div>;
}

export default FormBox;
