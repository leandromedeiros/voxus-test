import React from "react";
import "./style.scss";

function Title({ text }) {
  return <h1 className="title-default">{text}</h1>;
}

export default Title;
