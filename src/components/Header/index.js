import React from "react";
import "./style.scss";

function Header({ children }) {
  return (
    <header className="header-default">
      <div className="container">{children}</div>
    </header>
  );
}

export default Header;
