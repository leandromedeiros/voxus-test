import React from "react";
import "./style.scss";

function Text({ text }) {
  return <p className="text-default">{text}</p>;
}

export default Text;
